FROM alpine
ENV MONGO mongodb
ENV MONGO_PORT mongodb-port
RUN apk add --no-cache python2 \
     && apk add --no-cache --virtual build-dependencies git wget \
     && git clone https://github.com/express42/search_engine_ui.git \
     && wget -O get-pip.py https://bootstrap.pypa.io/pip/2.7/get-pip.py \
     && python get-pip.py \
     && ls -la ../search_engine_ui/ \
     && pip install -r ../search_engine_ui/requirements.txt \
     && pip uninstall -y pip \
     && apk del build-dependencies
WORKDIR /search_engine_ui/ui
ENTRYPOINT FLASK_APP=ui.py gunicorn ui:app -b 0.0.0.0
